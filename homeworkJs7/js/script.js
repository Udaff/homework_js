function createList (array){
    const mapArray = array.map(value =>{return `<li>${value}</li>`}
    );
    addElement(mapArray);
}

function addElement (mapArray) {
    let ul = document.createElement('ul');
    mapArray.forEach(element => {ul.insertAdjacentHTML("beforeEnd", element );});
    document.body.appendChild(ul);
    return mapArray;
}
createList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
createList(['1', '2', '3', 'sea', 'user', 23]);
function startTimer(duration, display) {
    let timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        display.textContent = minutes + ":" + seconds;
        if (--timer < 0) {
            document.body.innerText = '';
        }
    }, 1000);
}
window.onload = function () {
    let tenSec = 10,
        display = document.querySelector('#time');
    startTimer(tenSec, display);
};
