let color = document.querySelectorAll(".page-color");
let block = document.querySelectorAll(".block-image-hover");

let btn = document.createElement("button");
btn.innerText = "Change Theme";

color.forEach(element => {element.classList.toggle(localStorage.getItem("class"));});
block.forEach(element => {element.classList.toggle(localStorage.getItem("BGclass"));})
document.body.style.cssText = localStorage.getItem("bodyClass");

btn.addEventListener("click", ()=>{
if (localStorage.length>0){
window.location.reload();
localStorage.clear();
}else{
localStorage.setItem("bodyClass" , "background-image:url(img/newBgImage.jpg); color: black");
localStorage.setItem("class", "page-color-theme");
localStorage.setItem("BGclass", "block-image-hover-theme");
window.location.reload();
}
});
document.body.appendChild(btn);