 $("ul.tabs").on("click", "li:not(.tabs-title-active)", function() {
    $(this)
        .addClass("tabs-title-active")
        .siblings()
        .removeClass("tabs-title-active")
        .closest("div.centered-content")
        .find("li.tabs-text")
        .removeClass("tabs-text-active")
        .eq($(this).index())
        .addClass("tabs-text-active");
});


