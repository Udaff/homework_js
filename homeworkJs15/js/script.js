const $btn = $('.to-top-btn');
$btn.fadeOut();
$(document).scroll(function(e){
    let $clientHeight = $(window).innerHeight();
    if($(window).scrollTop() > 2*$clientHeight){
        $btn.fadeIn();
    } else {
        $btn.fadeOut();
    }
});
$btn.click(function(){
    $('html').animate({
        scrollTop: 0
    }, 500)
})
$('.hide-content-btn').click(function(){

    $('#block-hot-news').slideToggle(600)})




 $(document).ready(function(){
    $("#menu").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});
