let textBeforeField = document.createElement("p");
textBeforeField.innerText = "Цена, $: ";
textBeforeField.classList.add("form-text");
let error = document.createElement("p");
error.classList.add("error");
let field = document.createElement("input");
field.setAttribute("placeholder", "Price");
field.classList.add("form-input");
let form = document.createElement("form");
form.classList.add("form")


field.addEventListener("focus", () => {
    field.style.borderColor = "green";
});


field.addEventListener("blur", () => {
    if (field.value > 0) {
        field.style.borderColor = "grey";
        field.style.color = "green";
        let btn = document.createElement("div");
        btn.classList.add("span-button")
        btn.innerText = "x";
        btn.setAttribute("type", "button");
        let span = document.createElement("span");
        span.classList.add("span");
        span.innerText = `Текущая цена: ${field.value}`;
        span.appendChild(btn);
        if (document.getElementsByClassName("error").length > 0) {
            document.body.removeChild(error);
            form.before(span);
        } else {
            form.before(span);
        }
        btn.addEventListener("click", () => {
            document.body.removeChild(span);
            field.value = "";
            field.style.color = "grey";
        })
    } else {
        field.style.borderColor = "red";
        error.innerText = " Please enter correct price.";
        error.style.color = "red";
        form.after(error);
        field.value = "";
    }
});


form.appendChild(textBeforeField);
form.appendChild(field);
document.querySelector("script").before(form);