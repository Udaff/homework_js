let wrapper = document.getElementsByClassName("images-wrapper");
let count = 1;
let prevImg = 0;
let btnStop = document.getElementById("btn-stop");
let btnContinue = document.getElementById("btn-continue");
function slider (){
    wrapper[0].children[prevImg].style.display = "inline-block";
let interval = setInterval(()=>{
    wrapper[0].children[prevImg].style.display = "none";
    wrapper[0].children[count].style.display = "inline-block";
    prevImg = count;
    ++count;
    if (count >= wrapper[0].children.length){
        count =0 ;
    }
},1000);
btnStop.addEventListener("click",()=>{
    clearInterval(interval);
    btnStop.disabled = true;
    btnContinue.disabled = false;
});
}
btnContinue.addEventListener("click", ()=>{
    btnStop.disabled = false;
    btnContinue.disabled = true;
slider();
});

slider();


